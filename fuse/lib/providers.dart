import 'package:flutter/material.dart';

enum CardStatus { oui, non }

class CardProvider extends ChangeNotifier {
  Offset _position = Offset.zero;
  bool _isDragging = false;
  Size _screenSize = Size.zero;
  double _angle = 0;
  List<String> _linkImages = [];

  bool get isDragging => _isDragging;
  Offset get position => _position;
  double get angle => _angle;
  List<String> get linkImages => _linkImages;

  CardProvider() {
    resetQuestions();
  }

  void setScreenSize(Size screenSize) => _screenSize = screenSize;
  void startPosition(DragStartDetails details) {
    _isDragging = true;

    notifyListeners();
  }

  void updatePosition(DragUpdateDetails details) {
    _position += details.delta;

    final x = _position.dx;
    _angle = 45 * x / _screenSize.width;

    notifyListeners();
  }

  void endPosition() {
    _isDragging = false;
    notifyListeners();

    final status = getStatus();

    switch (status) {
      case CardStatus.oui:
        yes();
        break;
      case CardStatus.non:
        no();
        break;
      default:
        resetPosition();
    }
  }

  void resetPosition() {
    _isDragging = false;
    _position = Offset.zero;
    _angle = 0;

    notifyListeners();
  }

  void resetQuestions() {
    _linkImages = <String>[
      'assets/images/Question1.jpg',
      'assets/images/Question2.jpg',
      'assets/images/Question3.jpg',
      'assets/images/Question4.jpg',
      'assets/images/Question5.jpg',
      'assets/images/Question6.jpg',
      'assets/images/Question7.jpg',
      'assets/images/Question8.jpg',
      'assets/images/Question9.jpg',
      'assets/images/Question10.jpg',
    ].reversed.toList();
    notifyListeners();
  }

  CardStatus? getStatus() {
    final x = _position.dx;
    final y = _position.dy;

    final delta = 100;
    if (x >= delta) {
      return CardStatus.oui;
    } else if (x <= -delta) {
      return CardStatus.non;
    }
  }

  void yes() {
    _angle = 20;
    _position += Offset(2 * _screenSize.width, 0);
    _nextCard();

    notifyListeners();
  }

  void no() {
    _angle = -20;
    _position -= Offset(2 * _screenSize.width, 0);
    _nextCard();

    notifyListeners();
  }

  Future _nextCard() async {
    if (_linkImages.isEmpty) return;

    await Future.delayed(Duration(milliseconds: 200));
    _linkImages.removeLast();

    resetPosition();
  }
}
