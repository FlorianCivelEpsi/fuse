import 'package:flutter/material.dart';
import 'package:fuse/constants.dart';
import '../../Home/home_screen.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size; // hight and width of the screen
    return Container(
        height: size.height,
        width: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              top: size.height * 0.05,
              left: size.width * 0.25,
              width: size.width * 0.5,
              height: size.height * 0.5,
              child: Image.asset("assets/images/fusE..png"),
            ),
            const Text(
              "Bienvenue sur fusE.",
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            Positioned(
              bottom: size.height * 0.3,
              width: size.width * 0.5,
              child: SizedBox(
                height: 45,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: kPrimaryColor,
                    ),
                    onPressed: () {
                      runApp(Home());
                    },
                    child: const Text('Login',
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold))),
              ),
            ),
            Positioned(
              bottom: size.height * 0.2,
              width: size.width * 0.5,
              child: SizedBox(
                height: 45,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: kPrimaryColor,
                    ),
                    onPressed: () {},
                    child: const Text('Inscription',
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold))),
              ),
            ),
          ],
        ));
  }
}
