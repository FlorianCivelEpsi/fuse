import 'package:flutter/material.dart';
import 'package:fuse/Screens/Advice/advice_screen.dart';
import '../../main.dart';
import 'components/home_body.dart';
import '../Barcode/barcode_scanner.dart';

void main() {
  runApp(Home());
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        //use MaterialApp() widget like this
        debugShowCheckedModeBanner: false,
        home: HomeScreen() //create new widget class for this 'home' to
        // escape 'No MediaQuery widget found' error
        );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('   fusE.'),
        backgroundColor: Colors.green.shade900,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.directions_run_outlined),
            tooltip: '',
            onPressed: () {
              runApp(MyApp());
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.small(
        child: const Icon(Icons.bar_chart_outlined),
        backgroundColor: Colors.green.shade900,
        onPressed: () {
          runApp(Barcode());
        },
      ),
      body: HomeBody(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 1,
        backgroundColor: Colors.green.shade900,
        selectedItemColor: Colors.white,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.camera_alt_rounded),
            label: 'Scan',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.format_list_bulleted),
            label: 'Historique',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_fire_department_rounded),
            label: 'Bonnes pratiques',
          ),
        ],
        onTap: (int value) => {
          if (value == 0) {runApp(Barcode())},
          if (value == 2) {runApp(Advice())}
        },
      ),
    );
  }
}
