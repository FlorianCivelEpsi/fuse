import 'package:flutter/material.dart';
import 'package:fuse/Screens/Barcode/barcode_scanner.dart';
import 'package:fuse/Screens/Home/home_screen.dart';
import 'package:fuse/constants.dart';

class HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(scrollDirection: Axis.vertical, children: <Widget>[
      ListElement(
        classEng: 'F',
        marque: 'SAMSUNG',
        nom: 'TV4K',
        category: 'TV',
        conso: '99',
        color: Colors.red.shade900,
      ),
      ListElement(
        classEng: 'C',
        marque: 'LG',
        nom: 'LED 4JBI74H7',
        category: 'Lumière',
        conso: '13',
        color: Colors.yellow.shade600,
      ),
      ListElement(
        classEng: 'D',
        marque: 'Apple',
        nom: 'MacBook Pro 14 ',
        category: 'Ordinateur',
        conso: '64',
        color: Colors.orange.shade600,
      ),
      ListElement(
        classEng: 'A',
        marque: 'SONY',
        nom: 'LED intérieur  SONY',
        category: 'Lumière',
        conso: '7',
        color: Colors.green.shade900,
      ),
    ]);
  }
}

class ListElement extends StatelessWidget {
  final String classEng;
  final String marque;
  final String nom;
  final String category;
  final String conso;
  final Color color;
  bool isSelected;

  ListElement({
    super.key,
    required this.classEng,
    required this.marque,
    required this.nom,
    required this.category,
    required this.conso,
    required this.color,
    this.isSelected = false,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: GestureDetector(
        onTap: () => this.isSelected = !this.isSelected,
        child: FittedBox(
          alignment: Alignment.centerLeft,
          child: Material(
            color: this.isSelected ? Colors.black : Colors.white,
            elevation: 14.0,
            borderRadius: BorderRadius.circular(5),
            shadowColor: Colors.green.shade900,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Column(children: <Widget>[
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text(nom,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 3,
                                      fontWeight: FontWeight.bold)),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Consommation : ",
                                      style: TextStyle(fontSize: 2),
                                    )),
                                Container(
                                    child: Text(
                                  conso + " kWh/an",
                                  style: TextStyle(
                                      fontSize: 2, fontWeight: FontWeight.bold),
                                )),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                    child: Text(
                                  "Fabricant : ",
                                  style: TextStyle(fontSize: 2),
                                )),
                                Container(
                                    child: Text(
                                  marque,
                                  style: TextStyle(
                                      fontSize: 2, fontWeight: FontWeight.bold),
                                )),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                    child: Text(
                                  "Catégory : ",
                                  style: TextStyle(fontSize: 2),
                                )),
                                Container(
                                    child: Text(
                                  category,
                                  style: TextStyle(
                                      fontSize: 2, fontWeight: FontWeight.bold),
                                )),
                              ],
                            ),
                          ]))),
                  Container(
                    width: 17,
                    height: 17,
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(5),
                          bottomRight: Radius.circular(5)),
                      child: Container(
                        color: color,
                        padding: const EdgeInsets.all(4),
                        child: Text(
                          classEng,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 8,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}
