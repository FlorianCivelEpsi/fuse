import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:fuse/Screens/Advice/advice_screen.dart';
import 'package:fuse/Screens/Barcode/article.dart';
import 'package:fuse/main.dart';

import '../../constants.dart';
import '../Home/home_screen.dart';

void main() {
  runApp(const Barcode());
}

class Barcode extends StatelessWidget {
  const Barcode({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'fuse',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
      home: const MyHomePage(title: 'Barcode'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _scanBarcode = 'Unknown';

  Future<void> barcodeScan() async {
    String barcodeScanRes;
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Annuler', true, ScanMode.QR);
      print(_scanBarcode);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      barcode = barcodeScanRes;
    });
  }

  Future<void> startBarcodeScanStream() async {
    FlutterBarcodeScanner.getBarcodeStreamReceiver(
            '#ff6666', 'Cancel', true, ScanMode.BARCODE)!
        .listen((barcode) => print(barcode));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("   fusE."),
        backgroundColor: Colors.green.shade900,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.directions_run_outlined),
            tooltip: '',
            onPressed: () {
              runApp(MyApp());
            },
          ),
          //Image.asset("assets/images/logo.png"),
        ],
      ),
      floatingActionButton: FloatingActionButton.small(
        child: const Icon(Icons.edit),
        backgroundColor: Colors.green.shade900,
        onPressed: () {
          showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
              title: const Text("Sasie manuel"),
              content: const TextField(
                obscureText: false,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                  hoverColor: Color.fromARGB(255, 27, 94, 32),
                  border: OutlineInputBorder(),
                  labelText: 'code EAN',
                ),
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(ctx).pop();
                  },
                  child: Container(
                    padding: const EdgeInsets.all(14),
                    child: const Text(
                      "OK",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
      body: Builder(builder: (BuildContext context) {
        return Container(
            alignment: Alignment.center,
            child: Flex(
                direction: Axis.vertical,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(
                    height: 50,
                  ),
                  /*Text(
                    'Scan result : $_scanBarcode\n',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),*/
                  SizedBox(
                    height: 45,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Colors.green.shade900),
                        onPressed: () async =>
                            {await barcodeScan(), runApp(Article())},
                        child: const Text('Scanner un code à barre',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.bold))),
                  ),
                ]));
      }),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        backgroundColor: Colors.green.shade900,
        selectedItemColor: Colors.white,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.camera_alt_rounded),
            label: 'Scan',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.format_list_bulleted),
            label: 'Historique',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_fire_department_rounded),
            label: 'Bonnes pratiques',
          ),
        ],
        onTap: (int value) => {
          if (value == 1) {runApp(Home())},
          if (value == 2) {runApp(Advice())}
        },
      ),
    );
  }
}
