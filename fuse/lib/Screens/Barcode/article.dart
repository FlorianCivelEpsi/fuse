import 'package:flutter/material.dart';
import 'package:fuse/Screens/Advice/advice_screen.dart';
import 'package:fuse/Screens/Barcode/barcode_scanner.dart';

import '../Home/home_screen.dart';

void main() {
  runApp(const Article());
}

class Article extends StatelessWidget {
  const Article({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'fuse',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
      home: const MyHomePage(title: 'Article'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Description article scanner"),
        backgroundColor: Colors.green.shade900,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.navigate_before),
            tooltip: '',
            onPressed: () {
              runApp(Barcode());
            },
          ),
          //Image.asset("assets/images/logo.png"),
        ],
      ),
      floatingActionButton: FloatingActionButton.small(
        child: const Icon(Icons.add),
        backgroundColor: Colors.green.shade900,
        onPressed: () {
          runApp(Home());
        },
      ),
      body: Builder(builder: (BuildContext context) {
        Size size =
            MediaQuery.of(context).size; // hight and width of the screen
        return Container(
            height: size.height,
            width: double.infinity,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                  top: size.height * 0.05,
                  left: size.width * 0.25,
                  width: size.width * 0.5,
                  height: size.height * 0.35,
                  child: Image.network(
                      "https://static.fnac-static.com/multimedia/Images/FR/MDM/60/3e/f9/16334432/1540-1/tsp20221008113344/TV-Samsung-Crystal-50-LED-50AU7105-4K-UHD-Gris-anthracite.jpg"),
                ),
                Positioned(
                  top: size.height * 0.33,
                  //left: size.width * 0.36,
                  width: size.width * 0.5,
                  height: size.height * 0.5,
                  child: const Text(
                    "TV 4K Samsung HVF98J5",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Positioned(
                  top: size.height * 0.42,
                  //left: size.width * 0.36,
                  width: size.width * 0.5,
                  height: size.height * 0.5,
                  child: const Text(
                    "Ceci est une description. Elle ne sert pas à grand chose mais bon il faut bien combler le vide et donc ceci est un appareil de qualité qualitative et franchement jolie pour un grille pain.",
                    style: TextStyle(color: Colors.black, fontSize: 11),
                  ),
                ),
                Positioned(
                  top: size.height * 0.55,
                  //left: size.width * 0.36,
                  width: size.width * 0.5,
                  height: size.height * 0.5,
                  child: const Text(
                    "Classe énergétique : F",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 12),
                  ),
                ),
                Positioned(
                  top: size.height * 0.57,
                  //left: size.width * 0.36,
                  width: size.width * 0.5,
                  height: size.height * 0.5,
                  child: const Text(
                    "Consommation : 150 kWh",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 12),
                  ),
                ),
                Positioned(
                  top: size.height * 0.59,
                  //left: size.width * 0.36,
                  width: size.width * 0.5,
                  height: size.height * 0.5,
                  child: const Text(
                    "Note fuse : 6,7/10",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 12),
                  ),
                ),
                Positioned(
                  bottom: size.height * 0.10,
                  width: size.width * 0.4,
                  child: SizedBox(
                    height: 45,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.green.shade900,
                        ),
                        onPressed: () {
                          // runApp(Home());
                        },
                        child: const Text('Alternative',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold))),
                  ),
                ),
              ],
            ));
      }),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.green.shade900,
        selectedItemColor: Color.fromARGB(255, 17, 15, 15),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.camera_alt_rounded),
            label: 'Scan',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.format_list_bulleted),
            label: 'Historique',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_fire_department_rounded),
            label: 'Bonnes pratiques',
          ),
        ],
        onTap: (int value) => {
          if (value == 0) {runApp(Barcode())},
          if (value == 1) {runApp(Home())},
          if (value == 2) {runApp(Advice())}
        },
      ),
    );
  }
}
