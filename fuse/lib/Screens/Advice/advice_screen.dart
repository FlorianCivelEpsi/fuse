import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants.dart';
import '../../main.dart';
import '../../providers.dart';
import '../Barcode/barcode_scanner.dart';
import '../Home/home_screen.dart';
import 'components/question_card.dart';

void main() {
  runApp(Advice());
}

class Advice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => CardProvider(),
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
                primaryColor: Colors.white,
                elevatedButtonTheme: ElevatedButtonThemeData(
                    style: ElevatedButton.styleFrom(
                        elevation: 8,
                        primary: Colors.white,
                        shape: CircleBorder(),
                        minimumSize: Size.square(80))),
                scaffoldBackgroundColor: kPrimaryLightColor),
            //use MaterialApp() widget like this
            home: AdviceScreen() //create new widget class for this 'home' to
            // escape 'No MediaQuery widget found' error
            ));
  }
}

class AdviceScreen extends StatelessWidget {
  const AdviceScreen({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('   fusE.'),
          backgroundColor: Colors.green.shade900,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.directions_run_outlined),
              tooltip: '',
              onPressed: () {
                runApp(MyApp());
              },
            ),
            //Image.asset("assets/images/logo.png"),
          ],
        ),
        body: SafeArea(
            child: Container(
                color: Colors.white,
                alignment: Alignment.center,
                padding: EdgeInsets.all(16),
                child: Column(children: [
                  Expanded(child: buildCards(context)),
                  const SizedBox(height: 16),
                  buildButtons(context)
                ]))),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: 2,
          backgroundColor: Colors.green.shade900,
          selectedItemColor: Colors.white,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.camera_alt_rounded),
              label: 'Scan',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.format_list_bulleted),
              label: 'Historique',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.local_fire_department_rounded),
              label: 'Bonnes pratiques',
            ),
          ],
          onTap: (int value) => {
            if (value == 0) {runApp(Barcode())},
            if (value == 1) {runApp(Home())}
          },
        ),
      );

  Widget buildCards(context) {
    final provider = Provider.of<CardProvider>(context);
    final linksImages = provider.linkImages;

    return linksImages.isEmpty
        ? ListView(
            padding: EdgeInsets.all(24),
            children: [
              ListTile(
                title: Text(
                  '7/10',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 50),
                ),
                dense: true,
              ),
              ListTile(
                title: Text(
                  'Attention ! Pratiques de consommation à revoir sur les points ci-dessous.\n',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.red.shade700, fontSize: 20),
                ),
                dense: true,
              ),
              ListTile(
                title: Text(
                  "Question 3. Vous n\'éteignez pas vos appareils la nuit. \nQuestion 4. Vous ne débranchez pas vos appareils une fois chargés. \nQuestion 8. Vous ne dégivrez pas régulièrement vos congélateurs.",
                  textAlign: TextAlign.left,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
              ),
              ListTile(
                title: Text(
                  'Bravo ! vos pratiques de consommations sont bonnes sur ces points \n',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.green.shade900, fontSize: 20),
                ),
                dense: true,
              ),
              ListTile(
                subtitle: Text(
                  'Question 1. Vous faites vos machines aux heures creuses. \nQuestion 2. Vous couvrez vos casseroles. \nQuestion 5. Vous fermez les fenêtres quand le chauffage est en route.\nQuestion 6. Vous baissez le chauffage la nuit. \nQuestion 7. Vous utilisez des ampoules LED. \nQuestion 9. Vous utilisez la fonction "eco" de vos appareils. \nQuesiton 10.Vous ré-utilisez l\'eau de pluie pour arroser vos plantes.  ',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                dense: false,
              ),
            ],
          )
        : Stack(
            children: linksImages
                .map((linkImage) => QuestionCard(
                    image: linkImage, isFront: linksImages.last == linkImage))
                .toList(),
          );
  }

  Widget buildButtons(context) {
    final provider = Provider.of<CardProvider>(context);
    final linksImages = provider.linkImages;

    return linksImages.isEmpty
        ? Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                  onPressed: () {
                    final provider =
                        Provider.of<CardProvider>(context, listen: false);
                    provider.resetQuestions();
                  },
                  child: const Icon(
                    Icons.replay,
                    color: Colors.black,
                    size: 40,
                  )),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                  onPressed: () {
                    final provider =
                        Provider.of<CardProvider>(context, listen: false);

                    provider.no();
                  },
                  child: const Icon(
                    Icons.clear,
                    color: Colors.red,
                    size: 40,
                  )),
              ElevatedButton(
                  onPressed: () {
                    final provider =
                        Provider.of<CardProvider>(context, listen: false);

                    provider.yes();
                  },
                  child: const Icon(
                    Icons.favorite,
                    color: Colors.green,
                    size: 40,
                  ))
            ],
          );
  }
}
