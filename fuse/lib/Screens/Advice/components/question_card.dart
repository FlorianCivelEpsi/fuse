import 'package:flutter/material.dart';
import 'package:fuse/providers.dart';
import 'package:provider/provider.dart';

class QuestionCard extends StatefulWidget {
  final String image;
  final bool isFront;

  const QuestionCard({Key? key, required this.image, required this.isFront});

  @override
  State<StatefulWidget> createState() => _QuestionCardState();
}

class _QuestionCardState extends State<QuestionCard> {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final size = MediaQuery.of(context).size;

      final provider = Provider.of<CardProvider>(context, listen: false);
      provider.setScreenSize(size);
    });
  }

  @override
  Widget build(BuildContext context) => SizedBox.expand(
          child: Container(
        child: widget.isFront ? buildFrontCard() : buildCard(),
        height: 500,
      ));

  Widget buildFrontCard() => GestureDetector(
        child: LayoutBuilder(
          builder: (context, constraints) {
            final provider = Provider.of<CardProvider>(
              context,
            );
            final position = provider.position;
            final milliseconds = provider.isDragging ? 0 : 400;
            const double pi = 3.1415926535897932;

            final center = constraints.smallest.center(Offset.zero);
            final angle = provider.angle * pi / 180;
            final rotatedMatrix = Matrix4.identity()
              ..translate(center.dx, center.dy)
              ..rotateZ(angle)
              ..translate(-center.dx, -center.dy);

            return AnimatedContainer(
              curve: Curves.easeInOut,
              duration: Duration(milliseconds: milliseconds),
              transform: rotatedMatrix..translate(position.dx, position.dy),
              child: Stack(
                children: [
                  buildCard(),
                  buildStamps(),
                ],
              ),
            );
          },
        ),
        onPanStart: (details) {
          final provider = Provider.of<CardProvider>(context, listen: false);

          provider.startPosition(details);
        },
        onPanUpdate: (details) {
          final provider = Provider.of<CardProvider>(context, listen: false);

          provider.updatePosition(details);
        },
        onPanEnd: (details) {
          final provider = Provider.of<CardProvider>(context, listen: false);

          provider.endPosition();
        },
      );

  Widget buildCard() => ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            image: DecorationImage(
                fit: BoxFit.cover,
                alignment: Alignment.center,
                matchTextDirection: true,
                repeat: ImageRepeat.noRepeat,
                image: AssetImage(widget.image))),
      ));

  Widget buildStamps() {
    final provider = Provider.of<CardProvider>(context);
    final status = provider.getStatus();

    switch (status) {
      case CardStatus.oui:
        final child = buildStamp(color: Colors.green, text: "OUI");

        return Positioned(top: 10, left: 10, child: child);
      case CardStatus.non:
        final child = buildStamp(color: Colors.red, text: "NON");

        return Positioned(top: 10, right: 10, child: child);
      default:
        return Container();
    }
  }

  Widget buildStamp(
      {double angle = 0, required Color color, required String text}) {
    return Transform.rotate(
        angle: angle,
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: color, width: 4)),
            child: Text(text,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: color, fontSize: 48, fontWeight: FontWeight.bold))));
  }
}
